Pasos para la instalación

1.- Clonar el proyecto
2.- Ejecutar en la consola composer update
3.- configurar virtual host de la siguiente manera:
<VirtualHost *:80>
    ##ServerAdmin webmaster@dummy-host.example.com
    DocumentRoot "c:/rutapache/directorio"
    ServerName bank.loc
	<Directory "c:/rutapache/directorio">
	   Options All
	   AllowOverride All
	   Require all granted
	</Directory>
    #ServerAlias localhost
    #ErrorLog "logs/localhost.loc-error.log"
    #CustomLog "logs/localhost.loc-access.log" common
</VirtualHost>

4.- agregar el archivos host de windows la siguiente linea
127.0.0.1		bank.loc

5.- para pruebas, en el archivo .env del proyecto agregar o reemplazar los drivers de correo electronicos por estos
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=skyline.34.nissan@gmail.com
MAIL_PASSWORD=xcmsqevtspedxdja
MAIL_ENCRYPTION=tls
