
$(".btn-floating").on('click',function(){
    //$(window).scrollTop(0);
    scrollToTop(1000);
    $(this).hide('400');
});
$(window).scroll(function(){
    if($(window).scrollTop()>100){
        alert
        $(".btn-floating").show("600");
    }else{
        $(".btn-floating").hide();
    }
});

function scrollToTop(scrollDuration) {
    var scrollStep = -window.scrollY / (scrollDuration / 15),
    scrollInterval = setInterval(function(){
        if ( window.scrollY != 0 ) {
            window.scrollBy( 0, scrollStep );
        }
        else clearInterval(scrollInterval);
    },15);
}


var datatable = function(){
    var table = $(".grid-view");
    var campos = JSON.parse($(".grid-view").attr('data-campos'));
    console.log(campos);
    // return false;
    table.DataTable({
        "ajax":{
            "url":table.attr('data-ajax'),
        },
        language:esp,
        "serverSide":true,
        "bAutoWidth": false,
        "bProcessing": true,
        "order": [[1, "desc"]],
        "select": 'single',
        "columns": campos,
        initComplete:function(){
            reloadDataTable();
        }
    });//*/
}

function reloadDataTable(){
    $(".btn-refresh").on("click",function(e){
        e.preventDefault()
        $(".grid-view").dataTable().api().ajax.reload();
    });
}

var initializeSelect2 = function(){
    $('.select2').select2();
}

var listCatalogCurrencies = "";
if(document.getElementById("listCatalogCurrencies") != null){
    listCatalogCurrencies = document.querySelector("#listCatalogCurrencies");
    listCatalogCurrencies.addEventListener('change',function(){
        //alert(this.value);
        $(".grid-view").dataTable().api().search(this.value).ajax.reload();
    });
}

var validarFormularios = function(id_formulario){

    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-zñáéíóúäëïöüA-ZÑÁÉÍÓÚÄËÏÖÜ]+$/i.test(value);
    }, "<span class='text-danger'>Solo se permiten caracteres alfabéticos.</span>");
    /*Validar campos alfabéticos y espacios*/
    $.validator.addMethod("alpha_space", function (value, element) {
        return this.optional(element) || value == value.match(/^[a-zñáéíóúäëïöüA-ZÑÁÉÍÓÚÄËÏÖÜ ]*$/);
    }, "<span class='text-danger'>Solo se permiten caracteres alfabéticos y espacios en blanco.</span>");

    $.validator.addMethod("emptySelect", function(value, element, arg){
        return arg !== value;
    }, "Seleccione una opción");


    $(id_formulario).validate({
        rules: {
            nombre: {
                required: true,
                maxlength:45,
                lettersonly:true
            },
            descripcion: {
                minlength:10,
                maxlength:60,
                alpha_space:true
            },
            country_code: {
                required: true,
                maxlength:3,
                lettersonly:true
            },
            country: {
                required: true,
                maxlength:40,
                alpha_space:true
            },
            id_categoria:{
                required: {
                    depends(){
                        return $(this).val()==="";
                    }
                }
             }
        },
        messages: {
            nombre: {
              required: "<span class='text-danger'>Campo Obligatorio</span>",
              maxlength: jQuery.validator.format("El valor del campo supera el numero de caracteres permitidos {0}")
            },
            descripcion: {
              required: "<span class='text-danger'>Campo Obligatorio</span>",
              minlength: jQuery.validator.format("<span class='text-danger'>El valor del campo de contener como minimo {0} caracteres permitidos</span>"),
              maxlength: jQuery.validator.format("<span class='text-danger'>El valor del campo supera el numero de caracteres permitidos {0}</span>")
            },
            country: {
              required: "<span class='text-danger'>Campo Obligatorio</span>",
              maxlength: jQuery.validator.format("<span class='text-danger'>El valor del campo supera el numero de caracteres permitidos {0}</span>")
            },
            country_code: {
              required: "<span class='text-danger'>Campo Obligatorio</span>",
              maxlength: jQuery.validator.format("<span class='text-danger'>El valor del campo supera el numero de caracteres permitidos {0}</span>")
            },
            id_categoria: {
              required: "<span class='text-danger'>Seleccione una opción</span>",
            },

        },
        errorPlacement: function (error, element) {
            //check whether chosen plugin is initialized for the element
            if (element.data().chosen) { //or if (element.next().hasClass('chosen-container')) {
                element.next().after(error);
            } else {
                element.after(error);
            }
        }
    });
}


var esp = {
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Su consulta no arrojo resultados",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
}
