<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Citas extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = null)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('itsolutions@mailtrap.com')
        ->subject('AutoGestion de usuarios')
        ->attach("d:/www/comprobante.pdf",["as"=>"comprobante.pdf","mime"=>"application/pdf"])
        ->view('emails.acknowledgment')
        ->with('data',$this->data);
    }
}
