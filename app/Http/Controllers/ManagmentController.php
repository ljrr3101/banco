<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Mail\Citas;

class ManagmentController extends Controller
{

    public function rules()
    {
        return [
            'agencia' => 'required',
            'fecha' => 'required',
            'email' => 'required|email',
            'planilla'=>'required',
            'cedula' => 'required',
            'rif' => 'required',
            'ingresos' => 'required',
            "ref_personal"=>"required",
            "ref_bank"=>"required"
        ];
    }

    public function messages(){
        return $messages = [
            'required' => 'El Campo :attribute es obligatorio.',
            'email'=>"Indique una dirección de correo válida"
        ];
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('account_requirements');
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $formulario = $request->all();
        //dd($formulario);
        $validate=Validator::make($formulario,$this->rules(),$this->messages());
        if($validate->fails()){
            return redirect()->route('managment.create')->withErrors($validate,'citas')->withInput($request->input());
        }

        $data = array();

        try {
            \Mail::send('emails.acknowledgment', $data, function ($message) use($formulario) {
                $message->from('notificaciones@mibank.loc', 'MyBank.loc');
                $message->to($formulario['email'])->subject('Notificación');
                $message->attach("d:/www/comprobante.pdf");

            });//code...
            return back()->with('status','Se ha procesado solicitud, chequee su bandeja de correo electronico');
        } catch (\Throwable $th) {
            //throw $th;
            return back()->with('status',$th->getMessage());
        }
        // Mail::to('skyline.34.nissan@gmail.com')->send(new Citas());


    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }
}
