<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MailController extends Controller
{
    public function rules()
    {
        return [
            'agencia' => 'required',
            'fecha' => 'required',
            'email' => 'required|email',
            'planilla'=>'required',
            'cedula' => 'required',
            'rif' => 'required',
            'ingresos' => 'required',
            "ref_personal"=>"required",
            "ref_bank"=>"required"
        ];
    }

    public function messages(){
        return $messages = [
            'required' => 'El Campo :attribute es obligatorio.',
            'email'=>"Indique una dirección de correo válida"
        ];
    }

    public function sendMail(Request $request, $tpl = null){
        $data = $request->all();
        $formulario = $data;
        $validate=Validator::make($formulario,$this->rules(),$this->messages());

        if($validate->fails()){
            return redirect()->route('managment.create')->withErrors($validate,'citas')->withInput($request->input());
        }

        if($tpl=="contact"){
            try {
                \Mail::send('emails.contact', $data, function ($message) use($formulario) {
                    $message->from('Contacto@mibank.loc', 'Dev.MyBank.com');
                    $message->to($formulario['email'])->subject('Notificación');
                    //$message->attach("d:/www/comprobante.pdf");

                });//code...
                return back()->with('status','Se ha procesado solicitud, chequee su bandeja de correo electronico');
            } catch (\Throwable $th) {
                //throw $th;
                return back()->with('status',$th->getMessage());
            }
        }
    }
}
