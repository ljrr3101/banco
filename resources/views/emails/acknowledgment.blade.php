
<div class="container">
    <div class="row">
        <div class="col s6 offset-s3">
            <div class="card">
                <div class="card-content">
                    <h3>Portal de Autogestión de Usuarios</h3>
                    <p>Hemos Agendado su cita satisfactoriamente.</p>
                    <p>Para imprimir su cita haga clic <a href="{{ url('supports/comprobante.pdf') }}" target="_blank">aqui</a></p>
                    <p><b>Nota:</b>No olvide consignar los originales de los recaudos a efectos de conciliación bancaria</p>

                </div>
            </div>
        </div>
    </div>
</div>

