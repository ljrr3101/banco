<div class="footer-copyright cyan cyan-darken-3">
    <div class="container center-align">
        <span class="flow-text white-text">
            Todos los derechos reservados &copy; {{ date('Y') }}
        </span>
    </div>
</div>
