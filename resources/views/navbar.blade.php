<style>
    .nav-wrapper #nav-mobile li a{
        font-weight: 600;
    }
</style>
<ul id="dropdown1" class="dropdown-content">
    <li><a href="{{ route('managment.index') }}">Cuentas</a></li>
    <li><a href="#!">Creditos</a></li>
</ul>
<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper blue lighten-3 navbar-fixed">
            <a href="#" class="brand-logo"><img src="{{ asset('./img/logo.webp') }}" alt=""></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down text-black">
                <li><a href="{{ url('/') }}" class="text-black">Inicio</a></li>
                <li><a class="dropdown-trigger" href="{#!}" data-target="dropdown1">Personas<i class="material-icons right">arrow_drop_down</i></a></li>
                <li><a href="collapsible.html">Empresas</a></li>
            </ul>
        </div>
    </nav>
</div>
