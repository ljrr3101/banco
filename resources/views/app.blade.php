<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Portal de AutoGestion</title>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style>
        img.banner{
            width: 100% !important;
            height: 460px;
            object-fit: cover;
        }

        a.brand-logo img{
            max-height: 63.5px !important;
            width: 96px !important;
        }
        html body{
            background: #fafafa;
        }
    </style>
    @yield('css')
</head>
<body>
    @include('navbar')
    @yield('content')
    @include('footer')
    <!-- Compiled and minified JavaScript -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script>
        $(".dropdown-trigger").dropdown();
    </script>
    @yield('scripts')
</body>
</html>
