@extends('app')
@section('css')
<style>

    #recaudos{
        display: none;
    }
    .card-action{
        padding: 15px auto !important;
    }

    html {
        line-height: 1.5;
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
    }
    .materialert{
        position: relative;
        min-width: 150px;
        padding: 15px;
        margin-bottom: 20px;
        margin-top: 15px;
        border: 1px solid transparent;
        border-radius: 4px;
        transition: all 0.1s linear;
        webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
        box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .materialert .material-icons{
        margin-right: 10px;
    }
    .materialert .close-alert{
        -webkit-appearance: none;
        border: 0;
        cursor: pointer;
        color: inherit;
        background: 0 0;
        font-size: 22px;
        line-height: 1;
        font-weight: bold;
        text-shadow: 0 1px 0 rgba(255, 255, 255, .7);
        filter: alpha(opacity=40);
        margin-bottom: -5px;
        position: absolute;
        top: 16px;
        right: 5px;
    }
    .materialert.info{
        background-color: #039be5;
        color: #fff;
    }
    .materialert.success{
        background-color: #43a047;
        color: #fff;
    }
    .materialert.error{
        background-color: #c62828;
        color: #fff;
    }
    .materialert.danger{
        background-color: #c62828;
        color: #fff;
    }
    .materialert.warning{
        background-color: #fbc02d;
        color: #fff;
    }
</style>
@endsection

@section('content')
<div class="slider">
    <ul class="slides">
        <li>
            <img src="{{ asset('img/slideshow/transferencias.jpg') }}?{{ substr(time(),-5) }}"> <!-- random image -->
            <div class="caption left-align">
                <h3 class='black-text'>Canales electrónicos</h3>
                <h6 class="light black-text text-lighten-3" style="width: 350px; margin-left:0">Ponemos al alcance de tus manos una amplia gamma de canales electronicos para que realices tus operaciones desde cualquier lugar o dispositivo.</h6>
            </div>
        </li>
        <li>
            <img src=""> <!-- random image -->
            <div class="caption left-align">
                <h3>Respaldo y Solides Financiera</h3>
                <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
            </div>
        </li>
        {{--<li>
            <img src="https://lorempixel.com/580/250/nature/3"> <!-- random image -->
            <div class="caption right-align">
                <h3>Right Aligned Caption</h3>
                <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
            </div>
        </li>
        <li>
            <img src="https://lorempixel.com/580/250/nature/4"> <!-- random image -->
            <div class="caption center-align">
                <h3>This is our big Tagline!</h3>
                <h5 class="light grey-text text-lighten-3">Here's our small slogan.</h5>
            </div>
        </li> --}}
    </ul>
</div>
<div class="section white">
    <div class="row">
        <h3 class="center-align text-grey text-darken-1">¿Qué deseas Hacer?</h3>
        <div class="col s4 center-align">
            <i class="material-icons red-text">show_chart</i>
            <h4><a href="#!">Inversiones</a></h4>
            <h6 class="grey-text text-darken-1 flow-text" style="text-align:justify; width:50%; margin: 0 auto;">Con plazos flexibles para
                personas naturales y jurídicas
            </h6>
        </div>
        <div class="col s4 center-align">
            <i class="material-icons red-text">account_balance</i>
            <h4><a href="{{ route('managment.index') }}">Aperturas de Cuenta</a></h4>
            <h6 class="grey-text text-darken-1 flow-text" style="text-align:justify; width:50%; margin: 0 auto;">Soluciones que se adaptan
                a tus necesidades
            </h6>
        </div>
        <div class="col s4 center-align">
            <i class="material-icons red-text text-large">lightbulb_outline</i>
            <h4><a href="!#">Conocer lo Nuevo</a></h4>
            <h6 class="grey-text text-darken-1 flow-text" style="text-align:justify; width:50%; margin: 0 auto;">Conoce los nuevos productos
                a tu dispocisión
            </h6>
        </div>
    </div>
</div>
<div class="section grey-lighten-3">
    <div class="row">
        <div class="col l10 offset-l1">
            <h3 class="center-align text-grey text-darken-1">¿Qué Ofrecemos?</h3>
            <h6 class="text-grey text-darken-1">pone a tu disposición los 365 días del año, 24 horas al día nuestra plataforma en línea a través del cual puedes realizar tus operaciones de forma rápida y segura desde la comodidad de tu hogar u oficina.</h6>
            <div class="row">
                <div class="col m8">
                    <ul class="collection">
                        <li class="collection-item">
                            <h5><i class="material-icons red-text">check</i>AUTOGESTIÓN</h5>
                            <p>
                                Ahora tu mismo, sin necesidad de asistir a nuestras instalaciones, puedes hacer tus tramites totalmente en línea con la robustes y seguridad necesaria para tu tranquilidad
                            </p>
                        </li>
                        <li class="collection-item">
                            <h5><i class="material-icons red-text">check</i>MOVILIDAD</h5>
                            <P>Disfruta de todas las funcionalidades de nuestra en línea en cualquiera de tus dispositivos, sin limitaciones.</P>
                        </li>
                        <li class="collection-item">
                            <h5><i class="material-icons red-text">check</i>SEGURIDAD</h5>
                            <p>
                                A través de un mecanismo de validación de documentos con tecnología basada en Blockchain, puedes verificar tus operaciones y transacciones.
                            </p>
                        </ul>
                    </div>
                    <div class="col m4" style="background-image: url('{{ asset('img/smartphone.jpg') }}'); height:415px; background-size:cover; background-position:center; margin-top:8px;margin-bottom:85px;">
                        {{-- <img src="{{ asset('img/smartphone.jpg') }}" alt=""> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section white">
        <h3 class="center-align">Contactanos</h3>
        <div class="row">
            <div class="col s12 m10 l8 offset-m1 offset-l2">
                <form action="{{ route('mail_contact',["contact"]) }}" method="post">
                    @if (Session::has('status'))
                    <div class="materialert success">{{ Session::get('status') }}</div>
                    @endif
                    @csrf
                    <div class="input-field">
                        <input type="text" id="name" name="name">
                        <label for="" form="name">Nombre</label>
                    </div>
                    <div class="input-field">
                        <input type="text" id="subject" name="subject">
                        <label for="" form="subject">Asunto</label>
                    </div>
                    <div class="input-field">
                        <input type="email" id="email" name="email">
                        <label for="" form="email">Email</label>
                    </div>
                    <div class="input-field">
                        <textarea name="message" id="message" cols="30" rows="10" class="materialize-textarea"></textarea>
                        <label for="" form="message">Mensaje</label>
                    </div>
                    <div class="input-field center-align">
                        <button class="btn btn-small"> Enviar<i class="material-icons">send</i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="fixed-action-btn">
        <a class="btn-floating cyan lighten-2">
            <i class="large material-icons">arrow_drop_up</i>
        </a>
        {{-- <ul>
            <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>
            <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
            <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
            <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
        </ul> --}}
    </div>
    @endsection
    @section('scripts')
    <script src="{{ asset('js/funciones.js') }}?{{ substr(time(),-5) }}"></script>
    <script>
        $('.slider').slider();
    </script>
    @endsection
