@extends('app')
@section('css')
<style>

    #recaudos{
        display: none;
    }
    .card-action{
        padding: 15px auto !important;
    }

    html {
        line-height: 1.5;
        font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
    }
    .materialert{
        position: relative;
        min-width: 150px;
        padding: 15px;
        margin-bottom: 20px;
        margin-top: 15px;
        border: 1px solid transparent;
        border-radius: 4px;
        transition: all 0.1s linear;
        webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
        box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-box-align: center;
        -webkit-align-items: center;
        -ms-flex-align: center;
        align-items: center;
    }
    .materialert .material-icons{
        margin-right: 10px;
    }
    .materialert .close-alert{
        -webkit-appearance: none;
        border: 0;
        cursor: pointer;
        color: inherit;
        background: 0 0;
        font-size: 22px;
        line-height: 1;
        font-weight: bold;
        text-shadow: 0 1px 0 rgba(255, 255, 255, .7);
        filter: alpha(opacity=40);
        margin-bottom: -5px;
        position: absolute;
        top: 16px;
        right: 5px;
    }
    .materialert.info{
        background-color: #039be5;
        color: #fff;
    }
    .materialert.success{
        background-color: #43a047;
        color: #fff;
    }
    .materialert.error{
        background-color: #c62828;
        color: #fff;
    }
    .materialert.danger{
        background-color: #c62828;
        color: #fff;
    }
    .materialert.warning{
        background-color: #fbc02d;
        color: #fff;
    }
</style>
@endsection
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <img src="{{ asset("img/cuentas.jpg") }}" alt="" class="img-responsive banner">
        </div>
    </div>
    <div class="row">
        <div class="col s10 m8  l6 offset-s1 offset-m2 offset-l3">
            {{-- <h4 class="header">Agende su cita</h4> --}}
            <form name="frm_citas" name="frm_citas" action="{{ route('managment.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="card">
                    <div class="card-content">
                        @if (Session::has('status'))
                        <div class="materialert success">{{ Session::get('status') }}</div>
                        @endif
                        <span class="card-title activator grey-text text-darken-4">Agencia</span>
                        <div class="input-field">
                            <select name="agencia" id="agencia">
                                <option value="" disabled="disabled" selected="selected">Seleccione</option>
                                <option value="1">Agencia 1</option>
                                <option value="2">Agencia 2</option>
                                <option value="3">Agencia 3</option>
                            </select>
                            <label for="agencia">Agencia <small class="red-text">(*)</small></label>
                            <small class="red-text">{{ ($errors->citas->first('agencia')) ? $errors->citas->first('agencia') : "" }}</small>
                        </div>
                        <div class="input-field">
                            <input type="text" id="fecha" name="fecha" readonly class="datepicker">
                            <label for="fecha">Fecha <small class="red-text">(*)</small></label>
                            <small class="red-text">{{ ($errors->citas->first('fecha')) ? $errors->citas->first('fecha') : "" }}</small>
                        </div>
                        <div class="input-field">
                            <input type="text" id="email" name="email" >
                            <label for="fecha">Correo Electrónico <small class="red-text">(*)</small></label>
                            <small class="red-text">{{ ($errors->citas->first('email')) ? $errors->citas->first('email') : "" }}</small>
                        </div>
                        <span class="card-title activator grey-text text-darken-4">Formato de Solicitud</span>
                        <div class="file-field input-field">
                            <div class="btn btn-small">
                                <span><i class="material-icons">folder</i></span>
                                <input type="file" name="planilla" id="planilla" accept=".pdf">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" name="planilla_text" id="planilla_txt" type="text" placeholder="Cédula o Pasarporte">
                                <small class="red-text">{{ ($errors->citas->first('planilla')) ? $errors->citas->first('planilla') : "" }}</small>
                            </div>
                        </div>
                        <div id="recaudos">
                            <span class="card-title activator grey-text text-darken-4">Anexar Recaudos</span>
                            <div class="file-field input-field">
                                <div class="btn btn-small">
                                    <span><i class="material-icons">folder</i></span>
                                    <input type="file" name="cedula" id="cedula" accept=".jpg,.pdf">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" name="cedula_text" id="cedula_txt" type="text" placeholder="Cédula o Pasarporte">
                                    <small class="red-text">{{ ($errors->citas->first('cedula')) ? $errors->citas->first('cedula') : "" }}</small>
                                </div>
                            </div>
                            <div class="file-field input-field">
                                <div class="btn btn-small">
                                    <span><i class="material-icons">folder</i></span>
                                    <input type="file" name="rif" id="rif" accept=".pdf">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" name="rif_text" id="tif_txt" type="text" placeholder="Rif Vigente">
                                    <small class="red-text">{{ ($errors->citas->first('rif')) ? $errors->citas->first('rif') : "" }}</small>
                                </div>
                            </div>
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span><i class="material-icons">folder</i></span>
                                    <input type="file" name="ingresos" id="ingresos" accept=".pdf">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" name="ingresos_text" id="ingresos_txt" type="text" placeholder="Certificación de Ingresos">
                                    <small class="red-text">{{ ($errors->citas->first('ingresos')) ? $errors->citas->first('ingresos') : "" }}</small>
                                </div>
                            </div>
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span><i class="material-icons">folder</i></span>
                                    <input type="file" name="ref_personal" id="ref_personal" accept=".pdf">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" name="ref_personal_text" id="ref_personal_txt" type="text" placeholder="Referencias Personales">
                                    <small class="red-text">{{ ($errors->citas->first('ingresos')) ? $errors->citas->first('ingresos') : "" }}</small>
                                </div>
                            </div>
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span><i class="material-icons">folder</i></span>
                                    <input type="file" name="ref_bank" id="ref_bank" accept=".pdf">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" name="ref_bank_text" id="ref_bank_txt" type="text" placeholder="Referencias Personales">
                                    <small class="red-text">{{ ($errors->citas->first('ingresos')) ? $errors->citas->first('ingresos') : "" }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-action center-align" style="padding-bottom: 40px !important;">
                        <button class="btn btn-small cyan"><i class="material-icons">save</i>Guardar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js?{{ substr(time(),-5) }}"></script>
<script>
    let planilla = document.querySelector("#planilla");
    planilla.addEventListener('change',function(){
        if(this.value!=""){
            document.querySelector("#recaudos").style.display="block";
        }else{
            document.querySelector("#recaudos").style.display="none";
        }
    });


    const fecha = new Date();
    $('select').formSelect();
    $('.datepicker').datepicker({
        format:"dd-mm-yyyy",
        minDate:new Date(fecha.getFullYear(),fecha.getMonth(),fecha.getDate())
    });

    $("#frm_citas").validate({
        rules:{
            agencia:"required",
            fecha:"required",
            cedula_text:"required"
        },
        message:{
            agencia:"Seleccione una Agencia",
            fecha:"Debe seleccionar la fecha de la cita",
            cedula:"Debe Adjuntar este requisito",
        },
        errorElement : 'div',
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        }
    });
</script>
@endsection
