@extends('app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col s12">
            <img src="{{ asset("img/cuentas.jpg") }}" alt="" class="img-responsive banner">
        </div>
    </div>
    <div class="row">
        <div class="col s10 offset-s1">
            <p>La Cuenta Corriente Electrónica es una cuenta que permite movilizar los fondos las 24 horas del día y disfrutar de todos los servicios de la banca electrónica a través de la tarjeta de débito.</p>

            <h4>Características</h4>
            <div class="divider"></div>
            <ul class="collection">
                <li class="collection-item"><i class="material-icons">check</i> Es una cuenta corriente electrónica sin chequera.</li>
                <li class="collection-item"><i class="material-icons">check</i> Su único instrumento de movilización será Su Tarjeta de Débito.</li>
                <li class="collection-item">
                    <i class="material-icons">check</i> Disposición de los canales electrónicos las 24 horas del día, 365 días al año para realizar cómodamente sus transacciones.
                </li>
                <li class="collection-item">
                    <i class="material-icons">check</i> Aceptada en Puntos de Ventas, la Red de Cajeros Automáticos y otras redes interbancarias a nivel nacional.
                </li>
                <li class="collection-item">
                    <i class="material-icons">check</i> Dispone de Estado de Cuenta Digital, copia exacta de sus estados de cuenta habituales, disponible de forma simple, segura y más rápida a través del Portal en Línea sección Personas.
                </li>
                <li class="collection-item">
                    <i class="material-icons">check</i> Para realizar el trámite de apertura de cuenta, debe solicitar una cita en cualquiera de nuestras oficinas.
                </li>
            </ul>
            <p>
                <h4>Beneficios</h4>
            </p>
            <div class="divider"></div>
            <ul class="collection">
                <li class="collection-item"><i class="material-icons">check</i> Manejo de una cuenta de depósito sin necesidad de ir a una oficina.</li>
                <li class="collection-item" ><i class="material-icons">check</i> Disponer de un producto con la movilidad de una cuenta corriente.</li>
                <li class="collection-item"><i class="material-icons">check</i> Disponer de los canales electrónicos las 24 horas del día, 365 días al año para realizar cómodamente sus transacciones.</li>
                <li class="collection-item"><i class="material-icons">check</i> Operaciones que puede realizar a través de nuestros canales electrónicos</li>
                <li class="collection-item"><i class="material-icons">check</i> Ponemos a su disposición los canales electrónicos para que pueda acceder a sus productos y servicios de forma rápida, cómoda y segura desde la comodidad de su casa u oficina.</li>
            </ul>
            <p>
                <h4>Recaudos</h4>
                <ul class="Collection">
                    <li class="collection-item">
                        Formato de preapertura <a download href="{{ asset('supports/recaudos.pdf') }}"><i class="material-icons">cloud_download</i> Aqui</a>
                    </li>
                </ul>
                <div class="divider"></div>
                <p>Una vez llenada la planilla en, debe adjuntarla end formato pdf con los siguientes recaudos:</p>
                <ul class="collection">
                    <li class="collection-item"><i class="material-icons">check</i>Fotocopia Cédula de Identidad</li>
                    <li class="collection-item"><i class="material-icons">check</i>Fotocopia del RIF</li>
                    <li class="collection-item"><i class="material-icons">check</i>Una (1) Referencia Personal y dos (2) Bancarias o de Crédito (o la combinación de ellas)</li>
                    <li class="collection-item"><i class="material-icons">check</i>Acta Constitutiva (solo paraFirmas Personales)</li>
                </ul>
                <p>Para Agendar Su cita presione haga clic <a href="{{ route('managment.create') }}">Aqui <i class="material-icons">event_note</i></a></p>
            </p>
        </div>
    </div>
</div>
<div class="fixed-action-btn">
    <a class="btn-floating cyan lighten-2">
        <i class="large material-icons">arrow_drop_up</i>
    </a>
    {{-- <ul>
        <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>
        <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
        <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
        <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
    </ul> --}}
</div>
@endsection
@section('scripts')
<script>

</script>
@endsection
