<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
    //return redirect()->route('managment.index');
});

Route::post('/{tpl}/mail_contact',"MailController@sendMail")->name('mail_contact');

Route::group(['prefix' => 'gestion'], function () {
    Route::get("/","ManagmentController@index")->name('managment.index');
    Route::get("/crear","ManagmentController@create")->name('managment.create');
    Route::post("/agendar","ManagmentController@store")->name('managment.store');
});
